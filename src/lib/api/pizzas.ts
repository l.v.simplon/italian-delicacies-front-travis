export const get = async () => await (await fetch('https://italian-delicacies-staging.herokuapp.com/api/pizzas')).json()
export const add = async (pizzaName: string) => await fetch('https://italian-delicacies-staging.herokuapp.com/api/pizzas', {
	method: 'POST',
	body: JSON.stringify({ name: pizzaName }),
	headers: {
		'Content-Type': 'application/json'
	},
});

